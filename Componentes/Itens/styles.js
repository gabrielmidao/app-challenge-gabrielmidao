import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({

    frontContainer: {
        width:  Dimensions.get('window').width,
        height: Dimensions.get('window').height,
     //width: '100%',
      //height: '100%',
      },
    
      titles: {
        marginTop: '90%',
        width: '100%',
        alignItems: 'center',
        
      },
    
      title: {
        fontSize: 15,
        textAlign: 'center',
        letterSpacing: 6.67,
        color: '#FFFFFF',
        textTransform: 'uppercase',
        opacity: 1,
        fontFamily: 'serif',
      },
    
      subtitle: {
        fontSize: 35,
        textAlign: 'center',
        letterSpacing: 2.81,
        color: '#FFFF',
        textTransform: 'uppercase',
        opacity: 1,
        fontFamily: 'serif',
      },
  
      image: {
        width: '102%',
        height: '100%',
        resizeMode: 'cover',
        position: 'absolute',
      },
      
    //imagem key:5/6/7

      containerAlt: {
        width:  Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        backgroundColor: '#FFFF',
        alignItems: 'center',
        justifyContent: 'center',
       
      },
      
      imageT: {
        width: '82%',
        height: '82%',
        top: 60,
        right: 54,
        resizeMode: 'cover',
        position: 'absolute',
        opacity: 1,
      },
      titlesT: {
        marginTop: '105%',
        width: '100%',
        alignItems: 'center',
        
      },
      
      titleT: {
        fontSize: 13,
        textAlign: 'left',
        bottom: 383,
        right: 92,
        color: '#000000',
        textTransform: 'uppercase',
        opacity: 1,
        fontFamily: 'serif',
      },

      subtitleT: {
        fontSize: 13,
        bottom: 350,
        letterSpacing: 0,
        left: 125,
        textAlign: 'left',
        color: 'black',
        opacity: 1,
        fontFamily: 'notoserif',
      },
      subTituloS: {
        fontSize: 13,
        fontWeight: 'bold',
        bottom: 345,
        letterSpacing: 0,
        left: 106,
        textAlign: 'left',
        color: '#000000',
        opacity: 1,
        fontFamily: 'sans-serif-medium',
      },
      subTituleT: {
        fontSize: 12,
        bottom: 340,
        letterSpacing: 0,
        left: 129,
        textAlign: 'left',
        color: 'black',
        textTransform: 'uppercase',
        opacity: 1,
        fontFamily: 'notoserif',
        
      },

      titleL:{
        fontSize: 13,
        textAlign: 'left',
        bottom: 383,
        right: 105,
        color: '#000000',
        textTransform: 'uppercase',
        opacity: 1,
        fontFamily: 'serif',
      },

      //imagem key: 8
      titleZ: {
        fontSize: 13,
        textAlign: 'left',
        bottom: 395,
        right: 95,
        color: '#000000',
        textTransform: 'uppercase',
        opacity: 1,
        fontFamily: 'serif',
      },
      imageZ: {
        width: '82%',
        height: '82%',
        top: 60,
        right: 54,
        resizeMode: 'cover',
        position: 'absolute',
        opacity: 1,
      },
      
      subTitlePZ: {
        fontSize: 10,
        bottom: 350,
        letterSpacing: 0,
        left: 125,
        textAlign: 'left',
        color: 'black',
        textTransform: 'uppercase',
        opacity: 1,
        fontFamily: 'serif',
      },
      subTitleSZ: {
        fontSize: 10,
        bottom: 345,
        letterSpacing: 0,
        left: 103,
        textAlign: 'left',
        color: 'black',
        textTransform: 'uppercase',
        opacity: 1,
        fontFamily: 'serif',
      },
      subTitleTZ: {
        fontSize: 10,
        bottom: 340,
        letterSpacing: 0,
        left: 125,
        textAlign: 'left',
        color: 'black',
        textTransform: 'uppercase',
        opacity: 1,
        fontFamily: 'serif',
      },


      //Icones

      iconP: {
        bottom: 15,
        right: 154,
        backgroundColor: 'transparent',
        opacity: 0.4,
      },
      iconTitleP: {
        color:'black',
        fontSize: 10,
        bottom: 30,
        right: 95,
        textAlign: 'left',
        opacity: 1,
        fontFamily: 'serif',
      },
      iconS: {
        bottom: 15,
        right: 154,
        backgroundColor: 'transparent',
        opacity: 0.4,
      },
      iconTitleS: {
        color:'black',
        fontSize: 10,
        bottom: 30,
        right: 84,
        textAlign: 'left',
        opacity: 1,
        fontFamily: 'serif',
      },
      dash:{
        width: 170,
        height: 1,
        bottom: 20,
       right: 77,
      borderRadius: 30,
      }
    

});

export default styles;