import React from 'react';
import {View, Text, FlatList,Dimensions} from 'react-native';
import ModeloItem from '../Itens/index';
import styles from './styles';
import modelos from './modelos';



function ModelosLista (props) {
    return(
        <View style={styles.container}> 
            <FlatList 
                data={modelos}
                renderItem={({item}) => <ModeloItem teste={item}/> }
                pagingEnabled
                snapToAlignment={'start'}
                decelerationRate={'fast'}
                snapToInterval={Dimensions.get('window').height}
                //horizontal={true}
                //showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                />
        </View>
    );
};

export default ModelosLista;