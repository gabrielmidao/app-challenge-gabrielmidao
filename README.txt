### -- Intruções para rodar o código -- ###


.Instalar o node.js na maquina -- https://nodejs.org/en/ (versão LTS)

.Instalar o aplicativo "expo go" em um dispositivo movel a partir da
play store. Esse aplicativo será o responsavel por rodar o codigo direto no celular. 
Porfavor usar android.

.No terminal digitar  "npm install -g create-react-native-app" (sem aspas)

.Em seguida digitar "npm install --global expo-cli" e
depois digitar "npm install expo" para instalar 
o expo no projeto, bem como seus modulos. 
Deve ser digitado com o caminho da pasta contendo o projeto. 
O expo é o principal responsável pela apresentação do aplicativo.


.Digitar o comando "expo start" no terminal, ele irá criar um servidor local 
para rodar a aplicação, você vera um QR code e instruções aparecerem
no terminal e uma pagina no seu navegador será aberta automaticamente
contendo opções para rodar o app e um QR code. Nessa página deve-se trocar a 
opção LAN (marcada por padrão) para Tunnel no canto inferior esquerdo,
logo acima do QR code. 

.Após o retorno da mensagem "Tunnel ready", abra o aplicativo expo no
celular, na aba projects terá um botão "scan qr code", escaneie o
qr code na pagina que foi aberta, ele irá começar a montar o 
javascript no aplicativo, deverá levar aproximadamente 
um minuto para montar.

.O aplicativo estará carregado.

